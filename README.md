# Personal Website

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
[![Pipeline Status](https://gitlab.com/oclero/personal-website/badges/master/pipeline.svg)](https://gitlab.com/oclero/personal-website/commits/master)

Personal website source repository for <https://www.olivierclero.com>.

Made with [Hugo](https://gohugo.io) and using my custom theme [Supersonic](https://gitlab.com/oclero/supersonic) as a Git submodule.

**License:** The content of this website is licensed with [Creative Commons Attribution-NonCommmercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/), unless specified otherwise for specific content. In this case, it'll be specified on the content's page.

## Quickly run locally

1. First, ensure to pull the theme:

   ```bash
   git submodule update --init --recursive
   ```

2. Run Hugo's built-in HTTP server:

   ```bash
   hugo server
   ```

## Test in production environment

Instead of using Hugo's default `hugo server` command, we'll use Docker as it allows us to have an actual Apache server with PHP installed. The `docker-compose.yml` file contains the required configuration for that. Install [Docker](https://www.docker.com/), if not installed yet.

1. Run Hugo to generate your static website files to `./public` directory, and watch for changes.

   ```bash
   hugo --minify --watch
   ```

2. Run `docker-compose` to create a server that serves the `./public` directory to <http://localhost:8000/>.

   ```bash
   docker-compose up -d
   ```
