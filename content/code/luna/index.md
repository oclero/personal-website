---
title: Luna
date: 2021-07-17
Description: A modern and good-looking QML components library for desktop.
Tags:
  - qt
  - cpp
  - qml
  - open-source
thumbnail: cover.jpg
---

[Luna](https://github.com/oclero/luna) is a QML components library (Buttons, CheckBoxes, SpinBoxes, etc.) aimed to desktop usage.

I was surprised that there wasn't good QML components library for desktop. One can find mobile libraries but no desktop ones, unfortunately. I decided to make an attempt to learn QML and train myself for the Solarus Launcher UI library.

I hope that it will help people making desktop apps with QtQuick/QML.

- Based on QtQuickControls 2.
- **Fixes various behaviors** for default QML components.
- Adds support for a much needed `DoubleSpinBox`, to handle doubles (the standard one handles only integers).
- Provides a `TreeView`.
- Provides a `Vector3dEditor` to handle `QVector3D` objects.
- Lots of **animations** and **graphical effects**.
- Utilities like `ScrollUtils`, `GeometryUtils`, `BindingUtils`, `EventLoopUtils`...
- New components are not 100% pure QML: they're based on C++ classes for the backend and logic, for performance reasons.

{{< image "screenshot.png" >}}

### Information

- **Repository:** [https://github.com/oclero/luna](https://github.com/oclero/luna)
- **License:** [MIT](https://mit-license.org/)
