---
title: QtUpdater
date: 2021-11-11
Description: A C++ auto-update client for QtWidgets and QtQuick/QML applications.
Tags:
  - qt
  - cpp
  - open-source
thumbnail: cover.jpg
---

### Presentation

[QtUpdater](https://github.com/oclero/qtupdater) is a tool to check for updates in your Qt application. You may use it to check applications auto-updates, but not only.

The tool downloads an appcast (JSON file) from an endpoint and compares it with `QCoreApplication::applicationVersion()`. The JSON looks like this:

```json
{
  "version": "x.y.z",
  "date": "dd/MM/YYYY",
  "checksum": "418397de9ef332cd0e477ff5e8ca38d4",
  "checksumType": "md5",
  "installerUrl": "http://your-server/your-app/win/YourApp-x.y.z.t-Windows-64bit.exe",
  "changelogUrl": "http://your-server/your-app/win/YourApp-x.y.z.t--Windows-64bit.md"
}
```

No GUI is included, it is up to you to build your own in C++/QtWidgets or QML/QtQuick. The `QtUpdater` class has all necessary signals and slots to be used in one of these technologies.

It can:

- Check for update availability on a parameterized frequency (every start, every day...).
- Download changelog.
- Download installer.
- Check if the installer validates the MD5 checksum provided in the appcast.
- Launch the installer and quit the application.

It also comes with a `QtDownloader` utility that handles downloading a file to disk, or data, and exposes all these functionnality to QML.

### Usage

The basic usage looks like this:

```c++
// Create an updater.
oclero::QtUpdater updater("https://your-server/update-api/");

// Subscribe to all necessary signals. See documentation for complete list.
QObject::connect(&updater, &oclero::QtUpdater::updateAvailableChanged,
                 &updater, [&updater]() {
  if (updater.updateAvailable()) {
    qDebug() << "Update available! You have: "
      << qPrintable(updater.currentVersion())
      << " - Latest is: "
      << qPrintable(updater.latestVersion());
  } else {
    qDebug() << "You have the latest version.";
  }
});

// Start checking.
updater.checkForUpdate();
```

### Information

- **Repository:** [https://github.com/oclero/qtupdater](https://github.com/oclero/qtupdater)
- **License:** [MIT](https://mit-license.org/)
