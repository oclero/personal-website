---
title: Qlementine
date: 2024-01-01
Description: A QStyle for modern for Qt applications.
Tags:
  - qt
  - cpp
  - open-source
thumbnail: cover.jpg
---

### Presentation

[Qlementine](https://github.com/oclero/qlementine) is a modern `QStyle` for desktop Qt5/Qt6 applications.

As written in my [previous article](/code/custom-qstyle), a `QStyle` is a class that changes the look and feel of a Qt Application. Qlementine implements all the necessary API to give a modern look and feel to your Qt application. It's a drop-in replacement for the default `QStyle`.

**NB:** Also, I made [an icon library](https://github.com/oclero/qlementine-icons) to compliment this `QStyle`.

### Features

- A runtime-themable `QStyle` (yes, it supports light and dark themes);
- Animations for widget states and properties;
- Better performance than using Qt's CSS;
- Beautiful and modern new `QWidgets`;
- Utilites to use with `QPainter`.

### Usage

Define the `QStyle` on your `QApplication`.

```c++
#include <oclero/qlementine.hpp>

QApplication app(argc, argv);

auto* style = new oclero::qlementine::QlementineStyle(&app);
style->setThemeJsonPath(":/light.json");
QApplication::setStyle(style);
```

### Information

- **Repository:** [https://github.com/oclero/qlementine](https://github.com/oclero/qlementine)
- **License:** [MIT](https://mit-license.org/)
