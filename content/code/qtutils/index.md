---
title: QtUtils
date: 2022-02-04
Description: A set of tools to make using Qt5 more convenient.
Tags:
  - qt
  - cpp
  - open-source
thumbnail: cover.jpg
---

### Presentation

[QtUtils](https://github.com/oclero/qtutils) is a set of tools to make using Qt5 more convenient.

I was tired of implementing again and again the same utilities company after company, so I decided to make an open-source library with these commonly used tools, so everyone can enjoy them.

### Library Content

- `singleShotConnect`: Allows to create a single-shot `QMetaObject::Connection`.

  ```cpp
  oclero::singleShotConnect(this, &SomeClass::someSignalTriggered, []() {
    // Do stuff.
  });
  ```

- `QtScopedConnection`: RAII `QMetaObject::Connection` that will disconnect when destroyed.

  ```cpp
  oclero::QtScopedConnection scopedConnection = QObject::connect(this, &SomeClass::someSignalTriggered, []() {
    // Do stuff.
  });
  ```

- `QtEnumUtils`: Utilities to convert enums to `QString` or `int` and vice-versa.

  ```cpp
  auto str = oclero::enumToString(SomeEnum::SomeValue);
  auto value = oclero::enumFromString<SomeEnum>("SomeValue");
  ```

- `QtEventFilterUtils`: Utilities to quickly register to an event without the burden to create a `class`. All corresponding `QEvent`-derived classes have been mapped to their corresponding `QEvent::Type`, so you don't even have to cast the `QEvent` or worry about its type.

  ```cpp
  oclero::EventFilter<QEvent::MouseButtonPress>::install(watchedObject, [](QMouseEvent* e) {
    // Do stuff, then return 'true' to block the event propagation, 'false' otherwise.
    return false;
  });
  ```

- `QtDeleteLaterScopedPointer`: A pointer manager like `std::unique_ptr` or `QScopedPointer`, but calls `deleteLater()` instead of `delete`.

  ```cpp
  oclero::QtDeleteLaterScopedPointer<QObject> scopedPointer(rawPointer);
  ```

- `QtSettingsUtils`: Utilities to save and load **strongly typed** values from `QSettings`.

  ```cpp
  QSettings settings;
  auto value = oclero::loadSetting<int>(settings, "key", 3 /* default value */);
  oclero::saveSetting(settings, "key", 3);
  oclero::clearSetting(settings, "key");
  ```

### Information

- **Repository:** [https://github.com/oclero/qtutils](https://github.com/oclero/qtutils)
- **License:** [MIT](https://mit-license.org/)
