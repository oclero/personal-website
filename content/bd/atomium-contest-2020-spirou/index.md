---
title: 'Atomium Contest 2020: Spirou & Fantasio'
date: 2020-09-15
Description: My participation to Dupuis' Atomium Contest in 2020.
Tags:
  - spirou
  - contest
thumbnail: cover.jpg
---

During summer of 2020, for the first time, Dupuis organized a contest for Bruxelles Comic Strip Fest. Usually, the festival gives prices to professional authors and books, but this time they added a contest for amateur or not-yet-professional authors. The theme was: tell and draw a story (max 4 pages) with the famous characters Spirou and Fantasio. I immediately jumped on the occasion!

Unfortunately, I didn't had lots of time to make it. I planned to do it in August, every night after work. However, I wasn't able to do so. I eventually had to take two weeks of! So these 4 pages were done in two weeks: story, drawings, inking, colors. Every second counted!

I didn't win the contest, unfortunately. I guess it's because my story is not very good, because I'm rather confident in the drawings. I should have ask someone to read the storyboard before drawing it, but I didn't had enough time... Next year, I'll do better!

{{< image "atomium-2020-1.jpg" >}}
{{< image "atomium-2020-2.jpg" >}}
{{< image "atomium-2020-3.jpg" >}}
{{< image "atomium-2020-4.jpg" >}}
