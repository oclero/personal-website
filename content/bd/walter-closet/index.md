---
title: Walter Closet
date: 2016-01-12
Description: Walter Closet is a cadavre exquis I made with Arthur Couka.
Tags:
  - cadavre exquis
thumbnail: cover.png
---

## Context

In 2015, Arthur Couka and I began a _cadavre-exquis_ comic book. Here is the concept: every week, one after the other, we made comic strips to build progressively a story together, without having planned it before. The idea came after reading the fantastic collaboration between Brüno and Pascal Jousselin: [Les aventures de Michel Swing (coureur automobile)](https://www.glenat.com/treize-etrange/les-aventures-de-michel-swing-9782745917591). We loved the concept and wanted to have fun with it, so we made our own improvised story.

{{< image "walter-premier-ministre.jpg" >}}

## Realization

We created a Tumblr at <https://walter-closet.tumblr.com>, and without any other preparation than a quick brainstorming for the premise (politics), I drew the first strip. Our goal was to end every strip with a big suspense and a complicated situation. The following author had to drag out the character of their seemingly dramatic fate.

It was really interesting to do so, because it reminds me how series that were published in _Spirou magazine_ during the 40s/50s/60s did the same. It makes the story push froward consistently. Don't expect a solid scenario: the story is totally crazy and goes in every direction. But magically lands on its feet at the end!

Two ways to read the full story:

1. [The Tumblr website](https://walter-closet.tumblr.com)
2. [A PDF compilation](/pdf/walter-closet-album.pdf)

{{< image "page-11-1000px.jpg" >}}

Some guests made strips: Louis (Arthur's brother), Antoine (their father), and a longtime friend and professional artist [Yoann Hervo](https://www.behance.net/yoannhervo) (check out his art, he's fantastically fantastic). I am really grateful for their participation.

{{< image "page-47-1000px.jpg" >}}

More information:

- [The PDF we sent to publishers](/pdf/walter-closet-dossier.pdf)

## Final thoughts

It was an awesome experience. I still think the story is too much nonsense and some gags are not great, but having a project with a goal (76 pages) was really motivating. I struggle to write scenarios, and having to be totally free and draw a story without planning it was very liberating.

{{< image "irene-hegate.jpg" >}}
