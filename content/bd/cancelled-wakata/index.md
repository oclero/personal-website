---
title: L'Idole sacrée de l'île Wakata
date: 2015-12-01
Description: A cancelled short story I wanted to send to le Journal de Spirou.
Tags:
  - cancelled
thumbnail: cover.png
---

## Backstory

This is a long story.

I met Joris Chamblain a few years before he became successful in the comic books industry with _[Les Carnets de Cerise](https://fr.m.wikipedia.org/wiki/Les_Carnets_de_Cerise)_. We were working in summer camps: he was the summer camp director, and I was one of the summer camp leaders. Of course, it was a summer camp about comic books. What do you expect?

He was aleady writing stories and was about to become a profesional, and we decided to try to make a short story together (8 pages) that could fit in le Journal de Spirou. Graphism, tone, length: all was targeted to perfectly fit in my beloved comic books journal.

He imagined **_L'Idole sacrée de l'île Wakata_**: a story based on a misunderstanding between pirates and a young man they thought was a billionaire but actually was just a butler. The base story remained the same, but details were changed multiple times. I even met Jean-Claude Fournier, famous for drawing _Spirou & Fantasio_ during the 70s. He gave me some advice about panels clarity and story simplification.

Unfortunately, I got bored of restarting from scratch multiple times, and gave up. On his side, Joris Chamblain had well-deserved success with his other projects and went to other things.

## Story-board

The story-board you see here is the 5th one, after Fournier gave me some advice: less events, less dialogs and shorter ones, less stuff in each panel, etc. Globally I kept the original story though, slightly edited to be more fluid.

{{< image "wakata-decoupage.jpg" >}}

## Inking

I inked almost the whole story, with quill and black China ink, but I only scanned the first two pages. Here they are:

{{< image "wakata-encrage-p1.jpg" >}}
{{< image "wakata-encrage-p2.jpg" >}}

## Colors

Since I gave up, I never colorized the pages. I made some attempts at choosing a color theme, which I wanted warm and vintage. I used some grain to give texture, because I preferred the colors to be only flat tints. You can also see a reworked version of the first panel, with a fancy title and some even warmer colors.

{{< image "wakata-essai-couleurs-1.jpg" >}}
{{< image "wakata-essai-couleurs-2.jpg" >}}

## Final thoughts

I was almost bored of comic books, at that time, because I have drawn this short story five times! I guess I was not ready to become a professional.
