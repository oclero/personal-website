---
title: Miscellaneous Posters (2014-2020)
date: 2020-06-01
Description: Various posters I made for non-student events.
Tags:
  - poster
  - design
thumbnail: cover.png
---

I did not make posters only for student events. You will find here a small collection of my favorite
ones.

## Paco-Nut (2014)

Nathalie, a friend that I met during my studies, participated in a France-wide contest of food design. She's from South America, from Brasil precisely, so she wanted to involve this part of the world. She designed the whole chain, from producers to the product. She asked me to make a kakemono, that was used during the contest in Avignon, France, behind their stand. Here was the result!

{{< image "paco-nut.jpg" >}}

## Aroze (2015)

In 2015, the musician Aroze, from Rennes, contacted me to create his logo and make a poster for his upcoming concert. He's a violonist who mixes traditional celtic music with electronic dance music. His concerts are awesome moments, where everyone is dancing to his beats and violin melodies, under the futuristic neon aesthetics. I also made music album covers for him.

{{< image "aroze-concert.jpg" >}}

## Gay & Lussac's #5 (2017)

Back in the day, some friends in Paris were living together in a cool flat, in Gay Lussac street. They organized wild parties every 3 months or so, everytime with an imposed theme. This time, the highlight was Frida Kahlo's and Emmanuel Chain's legendary mono-eyebrow. Everyone was asked to wear a mono-eyebrow.

{{< image "gay-lussac-party-5.jpg" >}}

## Trophée des Étoiles 2017

Since 1988, an international feminine basketball tournament takes place in my origin city Montgermont: the _Trophée des Étoiles_. I already made posters for previous editions, but this time I wanted to really make something that shines. I created a brand new logo, and also created the whole graphic identity: videos, leaflets, books, posters, etc.

{{< image "trophee-des-etoiles-2017.jpg" >}}

## Trophée des Étoiles 2020 _(reported to 2021)_

Another edition of _Trophée des Étoiles_ was planned to happen in 2020, but the pandemic made us cancel the event. You may recognize a famous street from Rennes, behind the basketball players. I went for a less cartoony/childish look for the players. I also made videos, leaflets, etc. Everything is ready for when the tournament is rescheduled.

{{< image "trophee-des-etoiles-2020.jpg" >}}

## Final thoughts

Although I really like doing this kind of work, it takes me too much time and I would like to focus now on personal work! I don't take commissions like this anymore.
