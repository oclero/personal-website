---
title: Spirou Illustrations
date: 2019-10-06
Description: I love Spirou & Fantasio since my childhood, so I had some fun time drawing them.
Tags:
  - illustration
  - spirou
  - art
  - comics
thumbnail: cover.png
---

When I was a kid, I was fond of Tintin. _Objectif Lune_ and _On a marché sur la Lune_ were among the first BDs I read, and I immediately read all the other albums. There was also the legendary Tintin cartoon that aired on TV a the time, so I quickly new everything once could know about Tintin's adventure. Unfortunately, there are only 22 Tintin albums.

There come Spirou! I discovered it with _Les Voleurs du Marsupilami_ in my grandparents' attic. So I became a fan of the series, and spent hours reading the albums and drawing its characters. My dream was to become the official artist for the series. And still is. (Dupuis, please!).I grew up during Tome and Janry's run on the series, and also during the golden age of their spin-off _Le petit Spirou_ and the TV cartoon, so that would explain why I'm really attached to Spirou.

The series _Spirou & Fantasio_ is one the the most legendary franco-belgian BDs. Spirou is also the name of a magazine that was created in 1938 and still runs today. Spirou, despite having sold less than counterparts Tintin and Astérix, is fundamental for franco-belgian History. Its tone and universe is very similar to Tintin's, but more absurd, more fantastic, more fun! It goes where Hergé didn't want to go with Tintin.

## Spirou, d'après Fournier (2014)

Jean-Claude Fournier was the official artist for Spirou from 1969 to 1980. He was relatively unknown when he began. He had the hard task to follow André Franquin, who made Spirou the legendary series it is. Franquin was sort of a teacher for Fournier, and although Fournier's first albums were not excellent, the second half of his run is fantastic! He finally found his own style, and I love the way he drew his last two published Spirou albums. Chec Bizu, his own series, where his style really shines. Also, he was from Bretagne, so I felt very close to his albums. It made me realize it was possible to become Spirou's artist without being Belgian!

He began another one called _La maison dans la mousse_ (The House in the Foam) in 1980, that was very promizing, and more poetic than usual. However, tensions within Dupuis made him stopped the project, and he quitted the job. He already had drawn a few pages, and when I saw them, it was like finding a lost treasure. One of the strips is beautiful, and shows Spirou, Fantasio and Spip playing football in Champignac's garden. I loved it so much that I remade it, and colorized it.

{{< image "dapres-fournier.jpg" >}}

## Vacances à la plage (2017)

In 2017, during summer holidays, I was having fun with my quills, and decided to make a big illustration. It shows Spirou in one of Dinard's beautiful beaches, on Bretagne's North coast. A little detail: I just bought the Switch in 2017, and was carrying it everywhere. So, why wouldn't Spirou do the same?

{{< image "vacances-a-la-plage.jpg" >}}

## Homage to Philippe Tome (2019)

Philippe Tome (writing) and Janry (drawing) were the official Spirou artists from 1984 to 1998. Their run is considered the second golden age for Spirou (the first is Franquin's). I consider it THE golden age, maybe because I lived it. Spirou magazine was very dynamic at that time, with lots of good series and animations. After they lived/were pushed towards the exit, the series never shined as bright. It's like they took the magic with them. Tome was a master of telling compelling stories in only 46 pages. With Janry, he modernized Spirou & Fantasio while being faithful to the series. He'll remain one of the greatest writer that ever graced the franco-belgian universe, with smart plots, clever twists and hilarious humor. Rest in peace, M. Tome.

{{< image "hommage-to-philippe-tome.jpg" >}}
