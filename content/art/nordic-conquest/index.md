---
title: Nordic Conquest
date: 2012-12-01
Description: Nordic Conquest is an online RPG involving nordic mythology, which I was asked to make artwork for.
Tags:
  - video game
  - art
thumbnail: cover.png
---

Nordic Conquest is a project that was begun in 2012 by the French company Hecube, but cancelled. Benjamin Schweitzer, a friend who worked there, got the license rights, and decided to continue the project on his own. Most of the artwork I made for Nordic Conquest was when Hecube was developing it. Benjamin Schweitzer tried to resurrect the project in 2015, but got busy doing other projects. Currently, it's paused.

The game is similar to an online Card-RPG: think Magic, but as a website game, and also more simplistic. You could do a limited number of actions everyday, according to your current resources. You could also do some quests, i.e. mini-games, to get more resources. A map, that was planned to grow more and more, allowed the player to move its character. This is just a little part of the gigantic map I envisionned for the project. Unfortunately, I never had time to draw it all. The player was able to choose a tribe among these ones: humans, elves, dark elves, dwarfs and fairies.

{{< image "nordic-conquest-actions.jpg" >}}
{{< image "nordic-conquest-objects.jpg" >}}
{{< image "nordic-conquest-map.jpg" >}}
{{< image "nordic-conquest-characters.jpg" >}}
