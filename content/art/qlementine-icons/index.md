---
title: Qlementine Icons
date: 2023-12-31
Description: A sleek, elegant, open-source, vector icons C++ library for Qt apps.
Tags:
  - open-source
  - icons
  - design
thumbnail: cover.png
---

[Qlementine Icons](https://github.com/oclero/qlementine-icons) is a sleek, elegant and vector icon library. It features 350+ icons, covering the most common usages in software development. It is aimed to be paired with the usage of [Qlementine](https://github.com/oclero/qlementine).

The icons are licensed under the [MIT license](https://mit-license.org/) so can be used in a commercial app. If you need more i

### Preview

{{< image "all-qlementine-icons.png" >}}

### Qt Library

Currently, it comes as a C++ library for Qt applications, that exposes an Qt Icon Theme.

You can use the following methods to retrieve an icon:

1. With `QIcon::fromTheme()`, by using the icon name.

   ```cpp
   const auto icon = QIcon::fromTheme("redo");
   const auto pixmap = icon.pixmap(QSize(16, 16));
   ```

2. With `QIcon::fromTheme()`, by using the Freedesktop standard identifier, if the icon has one.

   ```cpp
   const auto iconName = oclero::qlementine::icons::fromFreeDesktop("edit-redo");
   const auto icon = QIcon::fromTheme(iconName);
   const auto pixmap = icon.pixmap(QSize(16, 16));
   ```

3. With QPixmap. Note that the resulting image will be 16×16 pixels.

   ```cpp
   const auto pixmap = QPixmap(":/qlementine/icons/redo.svg");
   ```

4. With QIcon, to get any size.

   ```cpp
   const auto icon = QIcon(":/qlementine/icons/redo.svg");
   const auto pixmap = icon.pixmap(QSize(64, 64));
   ```
