---
title: More Zelda games
date: 2017-12-10
Description: Illustrations and logos I did for the other Zelda games we made.
Tags:
  - zelda
  - solarus
  - design
  - art
  - video game
thumbnail: cover.png
---

## Mystery of Solarus XD (2011)

Notice the _XD_ and not _DX_: this game is a parodic _Zelda_ game and was released as an April's fool instead of _Mystery of Solarus DX_! I did not take part in the game's development, but only suggested ideas, notably the main plot: Link wakes up after a hangover, and has to find where Zelda is. Also, he lost his equipment (finally a good explanation why you have to find it again every time!).

[Download the game](https://www.solarus-games.org/en/games/the-legend-of-zelda-mystery-of-solarus-xd)

{{< image "zsxd-fake-movie-poster.jpg" >}}

## Return of the Hylian - Solarus Edition (2015)

This is a remake of Vincent Jouillat's first game that was originally released in 2006, remade entirely with Solarus to improve the gameplay.

[Download the game](https://www.solarus-games.org/en/games/the-legend-of-zelda-return-of-the-hylian-se)

{{< image "zroth.jpg" >}}

## Oni-Link Begins - Solarus Edition (2016 - TBA)

This is a remake of Vincent Jouillat's second game. The development is currently paused.

[More information](https://www.solarus-games.org/en/games/the-legend-of-zelda-onilink-begins-se)

{{< image "zolb.jpg" >}}

## XD2: Mercuris Chess (2018)

_Mercuris Chess_ is the follow-up to _Mystery of Solarus XD_ and was also released as an April's Fools in 2017. This is a parodic game too, full of stupid jokes and references to famous movies. If you play the French version, you'll see nods to _Les Inconnus_, _La Cité de la peur_, _Mission Cléopâtre_, _Rio ne répond plus_, etc. We tried to find equivalent jokes for the English culture.

[Download the game](https://www.solarus-games.org/en/games/the-legend-of-zelda-xd2-mercuris-chess)

{{< image "zxd2-boxart.jpg" >}}

{{< youtube M2tAk5hRMHk >}}
