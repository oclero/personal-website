---
title: VST 3 vector logo
date: 2023-10-07
Description: Say goodbye to the official low-res logo. Here is the vector one!
Tags:
  - logo
  - audio
  - design
thumbnail: cover.jpg
---

I was tired of seeing the **officiel low-res VST 3 logo** logo that everyone take from the official VST Github page.

So I made the **vector version**.

Here is the VST 3 logo in its full vector glory, and high resolution images:

- [Download ZIP with PNG and SVG files](vst3-logo.zip)

I made several variants (shaded, flat, only one color, framed, simplified).

No need to credit me if you use the images in your VST plugin, app or website. Steinberg, if you see this, feel free to use it on your website and Github repository.

Enjoy!

{{< image "vst3-logo-vector-full-framed.png" >}}

{{< image "vst3-logo-vector-full-black.png" >}}
