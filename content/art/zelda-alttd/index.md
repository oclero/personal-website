---
title: 'Zelda: A Link to the Dream'
date: 2018-12-01
Description: "A remake of the GameBoy cult Zelda game Link's Awakening."
Tags:
  - video game
  - zelda
  - solarus
thumbnail: cover.png
---

In 2018, we began the remake of one of the greatest games on GameBoy: Link's Awakening, originally published in 1993. This is a colossal work: the original game is huge, despite his tiny format! It is a SNES-like remake, made with Solarus. The release date is not announced yet: it'll be released when it is ready. Since we're working on it on our free time, it takes much longer than if we were full time on it.

{{< image "alttd-promo-poster.jpg" >}}

{{< image "alttd-characters.jpg" >}}
