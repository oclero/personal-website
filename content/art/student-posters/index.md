---
title: Student Posters (2008-2015)
date: 2015-12-01
Description: During my studies, I made a lot of posters for student parties. Here are some of my favorite ones.
Tags:
  - poster
  - design
thumbnail: cover.png
---

I studied Computer Science at the INSA (_National Institue of Applied Science_) of Rennes, France, from 2008 to 2013. It was really far from Art studies, so drawing posters was a good way of escaping, between . I learned graphic design by myself, progressively. Here are some of my favorite ones.

## Gala de l'INSA 2011

Every year, the school organizes a gala, where eveyone comes suited up. And every year, there is a theme. In 2011, the theme was _Travels_. The students organize a poster contest, and the only constraint is to fit the theme.

Here is what I made. This is the first I was very proud of. Obviously it's inspired by retro 60s aesthetics, and the fantastic movie _Catch Me If You Can_. This poster had so much success that I found out some students pinned it to their walls like a regular movie poster. I even remember that I autographed one in a student room!

{{< image "gala_insa_2011.jpg" >}}

## Rock N' Solex 2011

Rock N' Solex is a music festival organized since 1968 by the students. Originally, it was a rock festival, but nowadays it's only EDM, unfortunately. They also organize a poster contest every year, and I participated, but didn't win. I was very proud of the drawing, which is a nod to both Tank Girl by Jamie Hewlett and Steven Spielberg's famous E.T poster.

The art style I used for the character on the Solex is not my usual one, and I was very happy of the result. It is highly detailed, with heavy use of cross-hatches.

{{< image "rock_n_solex_2011_concours.jpg" >}}

## Rock N' Solex 2012

In winter 2012, I was studying in Helsinki, Finland (and what a cold winter!), so I was very far from home, without my drawing material. No pens, no paper, no A3 scanner, etc. And only a 11-inch underpowered computer with only Linux on it. I still wanted to participate to the poster contest, so I imagined something simple that didn't need a drawing. Yet, the title "Rock N' Solex" was drawn by hand, and photographed, then enhanced and vectorized. I was very proud of the result, considering how poor my hardware was. However, I lose again.

I wanted to combine the _dirt_ and the _rock_ sides of the festival, with respectively the tire marks and the guitar picks. I think the bold red and white would have been very striking as an advertizement on bus stops.

{{< image "rock_n_solex_2012_concours.jpg" >}}

## Fest-Noz de l'Agro 2014

My sister was studying at the same time at Agrocampus Ouest, And people from this school also organized poster contests. I particpated to the Fest-Noz poster contest, and I won everytime. This poster is not my first one, that was uglier, but the first for which I remade the logo.

The character is drawn in my classic franco-belgian style a.k.a. 'Spirou style'. My idea was that this style of poster would be used as a template for the following festival editions.

{{< image "fest_noz_agro_2014.jpg" >}}

## Garden Party du BREI de Rennes 2014

The Garden Party du BREI was an event that gathered all engineering school students from Rennes, with sport competitions, games, concerts and parties. This was the first time the event was held, and they asked me to create a poster for it.

I spent a lot of time doing the grass-covered letters, and I like the result. A painted cow statue was the mascot of the event, so I had the idea of drawing her in a cartoon style, as a nod to the _Cow and Chicken_ cartoon that aired on FR3 when I was a kid (do you remember it?).

{{< image "garden_party_du_brei_rennes_2014.jpg" >}}

## Fest-Noz de l'Agro 2015

Once again (it was maybe the forth or fifth time), I made the poster for the Fest-Noz de l'Agro. I reused the logo I made for the previous edition, and followed the same template: logo + breton-dressed girl. This time, I decided to experiment a bit with the drawing style, with something influenced by Benoît Feroumont (I was really enjoying his series _Le Royaume_ at that time, and I still do).

{{< image "fest_noz_agro_2015.jpg" >}}

## Garden Party du BREI de Rennes 2015

This was the last poster I made as a student. Hey! I wasn't even student anymore at this time! And I was living in Lille, not even in Rennes! My studies finished 2 years earlier, but I kept contact with people still students, and they asked me to make the Garden Party poster.

Since this was my last student party poster, I wanted to do my best and create the most memorable party poster I was capable of. I reused the grass-made logo I did for the previous year and spent hours thinking about what could be striking. I ended up using negative space to show the cow head (event mascot), and drawing flowers all over the poster. The original drawing was done on an A4 paper, and I vectorized it to make it look good on huge posters. A filter and a texture are applied to make it look dirtier. This is my favorite student party poster.

{{< image "garden_party_du_brei_rennes_2015.jpg" >}}

## Final thoughts

These posters are only my favorite ones of this period of time. Actually, I made more than 20 posters over 5 years. It was a really fun experience, and I learned a lot. It taught me skills in a field that wasn't taught by the engineering school. I met lots of awesome people, and some still are among my best friends. And thanks to these posters, I almost never paid a party during my studies! Here's a full recap of (almost) all posters I made, or at least the ones I can still find in my HDD.

{{< image "all-posters.jpg" >}}
