---
title: Solarus
date: 2011-12-02
Description: Solarus is a 2D game engine originally made for Zelda-like games.
Tags:
  - solarus
  - design
  - brand identity
  - video game
thumbnail: cover.png
---

[Solarus](https://www.solarus-games.org/) is a 2D game engine originally made for Zelda-like games. I am involved in the project for a long time, before Solarus wasn't even called Solarus, and was still in the head of Christopho, its creator.

## Game engine logo (2011)

This logo was one of the first logos I made. It was a long process, because I was an Adobe Illustrator noob at that time. I tried to make something that feels 90s old-school.

Today, I regret making the tagline part of the logo. I'm working on improving this, stay tuned!

{{< image "logo-solarus-engine.png" >}}

## Software logos (2011)

Solarus is the game engine, and you have some tools to use it. As a player, you browse and launch games with Solarus Launcher. And as a game developer, you create your games with Solarus Quest Editor.

{{< image "logo-solarus-launcher.png" >}}

{{< image "logo-solarus-quest-editor.png" >}}

## Website engine logo (2019)

In 2019, we replaced the old Wordpress website with a custom website backend that we calld Kokori. The name is a nod to Kokiri, the name of the forest elfs in _The Legend of Zelda: Ocarina of Time_, and Coq-au-riz, Binbin's famous recipe.

The logo is a 'K' based on the Kokiri symbol that we can see on the Kokiri shield.

{{< image "logo-kokori.png" >}}

## Nonprofit organization logo (2020)

In 2020, we created a nonprofit organization to support the project and receive donations legally. It's called Solarus Labs. Now you can donate legally, and even, if you're French, deduce the amount from your taxes!

{{< image "logo-solarus-labs.png" >}}

## Game development team (2017)

The historical team behnd Solarus, led by Christopho, needed a logo to make it distinct from Solarus itself. Since Christopho is fond of chess, the logo uses its distinctive elements, also suggesting distinctive elements from _Zelda_ games.

{{< image "logo-solarus-team.png" >}}

## Icons (2018)

This is an icon collection I made for the project.

{{< image "solarus-icons.png" >}}
