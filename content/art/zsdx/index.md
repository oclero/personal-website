---
title: 'Zelda: Mystery of Solarus DX'
date: 2011-12-01
Description: 'The very first game made with Solarus: a follow-up to Zelda A Link to the Past.'
Tags:
  - video game
  - zelda
  - solarus
thumbnail: cover.png
---

## A bit of History

In 2002, a very young Christopho created a very amateurish _The legend of Zelda_ fangame with RPG Maker: _Mystery of Solarus_. It was designed as a follow-up to _The Legend of Zelda: A Link to the Past_. In 2006, he decided to remake it with an actual dedicated game engine that he called [Solarus](https://www.solarus-games.org/).

The remake was released in 2011, and since it is a _deluxe_ edition with more refined gameplay, graphics and musics, it was called _Mystery of Solarus DX_. At the time, I was not yet totally commited to the project, so I did not particpat in game development, apart from the title screen, but rather helped the project having a fancy visual identity that would help giving it the more professional look it deserved.

The game is available to download for free on [Solarus game library](https://www.solarus-games.org/en/games/the-legend-of-zelda-mystery-of-solarus-dx). Be sure to download the launcher before, to be able to launch the game.

## Artwork for the game

Since it is a love-letter to the glorious days of _Zelda_ on _Super Nintendo_, I designed box art for the game. A full box template was printable, if you wanted to recreate the box.

{{< image "zsdx-boxart.jpg" >}}

I made lots of artwork that was used to promote the game, and illustrate the game manual, that was done with the long-lost art of game manuals. They were also used in a complete walkthrough that was distributed as PDF, and mimicked the big walkthrough books of the 1990s and early-2000s.

{{< image "zsdx-link.jpg" >}}

As you may see, I was very under Peyo's Johan-and-Pirlouit-era influence, at that time!

{{< image "zsdx-characters.jpg" >}}

{{< image "zsdx-monsters.jpg" >}}

{{< image "zsdx-objects.jpg" >}}

The game's introduction scene told the events of _A Link to the Past_ with frescos in the style of the ones you can see in the introduction of the cult classic _The Wind Waker_.

{{< image "zsdx-frescos.jpg" >}}

## Final thoughts

This project was very beneficial for me: I learned how to use Adobe Illustrator by making the logo for this game, I learned how to use Adobe InDesign by making the game manual and walkthrough for this game, and more than anything, I became very good friends with the rest of the team that made the game. I would eventually collaborate with them on more games!

The game _Mystery of Solarus DX_ has been downloaded over 2 million times since 2011, and is widely considered as one of the most accomplished _Zelda_ fangames. Solarus, the engine that powers it, has continued its road, and after having distanced itself from pure _Zelda_, is becoming progressively a more universal 2D game engine, with a vibrant and very helpful community.
