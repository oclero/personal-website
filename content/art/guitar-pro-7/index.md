---
title: Guitar Pro 7
date: 2017-12-01
Description: Beside participating in Guitar Pro 7's development, I made its graphic design.
Tags:
  - logo
  - software
  - design
  - ux-ui
thumbnail: cover.png
---

## What is Guitar Pro?

Guitar Pro is a famous tablature and music notation software, created by French people from Lille in 1997. It is widely used by millions of guitar players because of its small file format and convenient user experience. You can find Guitar Pro files for almost any song.

I was a long-time user of Guitar Pro when I joined the company in 2013, and we began working on the seventh version. I was hired to be a developer, but since I really like doing graphic design and there wasn't a graphic designer in-house in the company, I quickly got this task too.

## Logo and icon

I made the Guitar Pro 7 logo and icon.

The logo not a big departure from Guitar Pro 6's logo. It is just a refresh with new flat colors. The blue was decided to replace GP6's red color.

{{< image "gp7-logo.png" >}}

For the icon, the goal was to make a sleek, modern logo, while keeping the iconic guitar pick. The _gp_ letters stand for Guitar Pro and are written with the official Guitar Pro font.

{{< image "gp7-icon.png" >}}

## Box art

I also designed the digipack boxart for Guitar Pro 7. This was actually printed, it's not just a mockup! What a pleasure when I received the DVD box!

{{< image "gp7-digipack.png" >}}

## Software design

As a developer, building the UI was my main task. It was a huge and long task, but I'm proud of the result. We used C++/QtWidgets and QSS (Qt's _very_ limited subset of CSS), so the UI couldn't be as fancy as I wanted to. However, based on the limited possibilites of QtWidgets, the result is quite modern.

### Home screen

{{< image "gp7-screen-1.png" >}}

### Main screen

{{< image "gp7-screen-2.png" >}}

### Score browser

{{< image "gp7-screen-3.png" >}}
{{< image "gp7-screen-4.png" >}}

### Chord library

{{< image "gp7-screen-5.png" >}}

### Tuning

{{< image "gp7-screen-6.png" >}}

### Track creation

{{< image "gp7-screen-7.png" >}}

## Final thoughts

I am very proud of what we accomplished as a team. Arobas Music people are talented, great and passionate. It was a pleasure to work with them, and I learned a lot: maybe as much in software development as in graphic design.
