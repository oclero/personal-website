---
title: Album Covers for Three Blind Mice
date: 2021-12-12
Description: Three Blind Mice is a French New Orleans jazz band I made album covers for.
Tags:
  - album cover
  - design
thumbnail: cover.png
---

I know [Malo Mazurié](https://www.malomazurie.com/) from high school. He's a professional trumpet player, and his predilection genre is New Orleans jazz. After high school, he began his career and asked me to make his first album cover. Since then, I've almost made all his album covers.

He created the band [Three Blind Mice](https://www.threeblindmice.fr/) with equally awesome musician friends Félix Hunot (guitar) and Sébastien Girardot (double bass). (No drummer, yes! And it works!) They have made 3 albums so far. If you like this kind of old swing music, I strongly recommend to buy their albums or to see them in concert. Here is their [bandcamp page](https://threeblindmiceparis.bandcamp.com).

## Three Blind Mice (2016)

This is their first album. The cover is actually an rework/improvement of a cover idea made by Cécile McLorin-Salvant, a famous jazz singer. I really liked her (unintentional?) kind-of papercut style, so I tried to improve it with subtle shadows and texture. Also, I brightened the colors to make it less dull. Cécile made a great, impactful minimalistic cover.

{{< image "tbm-first-album.jpg" >}}

## See How They Run (2018)

For their second album, they wanted me to draw something and keep the line-art. So I kept my classic franco-belgian style and tried to move up the 'realism' knob a bit. I really like the mysterious and classy framing. Also, I tried to paint with watercolor-like brushes in Photoshop. And the mice are so cute!

{{< image "tbm-see-how-they-run.jpg" >}}

## A Swinging Christmice (2020)

Every good band HAS to have a Christmas album, and Three Blind Mice won't infrige this rule! They covered Christmas standards with their old swing style, and it works very well! They asked me to do something close to 60s child illustrations/cartoons, with no line-art this time, only flat colors. I couldn't resist to add subtle shadows, as if it was papercuts. In my first attempt, I used too many colors, so they asked me to use a limited set, which was a great idea and works wonderfully!

{{< image "tbm-a-swinging-christmice.jpg" >}}

## Silence, Action... Jazz ! (2021)

_Silence, Action... Jazz !_ is a concert-movie project by Three Blind Mice. The band plays the soundtrack (music and sound effects!) of classic silent movies. This is the poster I made for them.

{{< image "tbm_silence_action_jazz_poster.jpg" >}}
