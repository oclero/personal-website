---
title: Allflowers - Singles
date: 2023-12-01
Description: Singles taken from Allflowers' first album, including music videos.
Tags:
  - single
  - allflowers
thumbnail: cover.png
---

We released several singles from our first album [More or Less Forever](/music/allflowers-more-or-less-forever). Here they are, in a chronological order.

### Somewhere Between the Lines (05/2022)

{{< image "somewhere-between-the-lines.jpg" "300x300" >}}

This was originally an instrumental composition by Olivier (2012).

Lara was inspired by the instrumental and added lyrics and vocals. Olivier liked it so much that he totally re-recorded the instruments.

It became a pop-rock ballad with a dream-pop touch, reminiscent of the 90s bands such as The Cranberries.

{{< youtube-enhanced id="yyVhakmMVbY" title="Somewhere Between the Lines" >}}

### Tired of Pretending (07/2023)

{{< image "tired-of-pretending.jpg" "300x300" >}}

This was originally an instrumental composition by Olivier (2016) that set a speech by Charlie Chaplin to music. It was gradually transformed to its final form. The choruses were found quickly, while the verses took longer to take shape.

A darkly atmospheric piece with spoken verses, sung choruses (inspired by Massive attack), and a post-rock instrumental finale inspired by Radiohead and the Beatles’ _I Want You (She’s So Heavy)_.

{{< youtube-enhanced id="cXBs700IBog" title="Tired of Pretending" >}}

### Zagreb Song (10/2023)

{{< image "zagreb-song.jpg" "300x300" >}}

Composition by Lara (2018) with the particularity of not having a chorus.

The ballad was inspired by Lara’s travels in her childhood to Croatia, her family’s homeland, and composed in Zagreb during the summer of 2018.

The song took time to find its final form: it went through several different styles before revealing itself in an acoustic pop-folk form.

The instrumental finale is inspired by Lucky Man by _The Verve_.

{{< youtube-enhanced id="w2PK0eooleM" title="Zagreb Song" >}}
