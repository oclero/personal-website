---
title: Allflowers - Winter Letter
date: 2023-12-20
Description: Allflowers' original Christmas song, an orchestral popsong about the end-of-season warm atmosphere.
Tags:
  - single
  - allflowers
thumbnail: cover.png
---

{{< image "winter-letter.jpg" "300x300" >}}

Writing the song was a challenge: what if we created our own Christmas song?

We had two weeks ahead of us to make it, and wanted to avoid the usual cheesiness of this kind of song, so we worked hard, and here it is! We’ve tried to capture the generous and benevolent atmosphere of Christmas, with traditional instruments, impressionistic lyrics and exuberant choruses. We hope you enjoy it!

More detailed information on [our band official website](https://www.allflowers-music.com/).

### Listen

{{< youtube-enhanced id="5R9tmKER-W0" title="Winter Letter" >}}

### Credits

Band:

- Composition, Lyrics, Vocals: **Lara Martinovic**
- Composition, Guitars, Mandolin, Bass, Arrangements, Production: **Olivier Cléro**

Special guests:

- Violin: **Cécile Passaquay**
- Cello: **Magali Cadas**

Album cover:

- Design: **Olivier Cléro**
