---
title: Miscellaneous Covers
date: 2021-03-20
Description: My friends and I covering songs we like.
Tags:
  - covers
  - tribute
thumbnail: cover.jpg
---

Although we work with our own compositions, we also like to play covers of songs we like. Here are some home recordings just for fun.

Cover picture credits: Lara Martinovic.

{{< audio "lara-and-olivier-after-hours.mp3" "Lara & Olivier - After Hours (Velvet Undergound cover)" "lara-and-olivier-after-hours.jpg" >}}
