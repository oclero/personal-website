---
title: Allflowers - More Or Less Forever
date: 2021-11-11
Description: Allflowers' first album, a blend of 90s pop and alternative rock.
Tags:
  - album
  - allflowers
thumbnail: cover.jpg
---

### A bit of context

{{< image "album-front.jpg" >}}

In early 2018, I met Lara at my sister's birthday. She told me I should talk to her because she's into music too. So we chat a bit, and thought it would be cool to record some music together. And in late 2018, when I felt the urge to play music, I tried to re-work one of Lara's own compositions: Emptiness.

She liked it very much and we decided to re-work more demos she had. I also had some instrumental tracks where she found lyrics and melody. After several years of dilletante work and multiple from-scratch remakes of our songs, here it is, our first album!

The influences are mostly taken from the 90s alternative rock scene: Cranberries (a band that I loved as a kid and still do, and that Lara absolutely loves too), Smashing Pumpkins, The Verve, Cocteau Twins...

More detailed information on [our band official website](https://www.allflowers-music.com/).

### Listen

{{< bandcamp "3185643679" >}}

### Credits

Band:

- Composition, Lyrics, Vocals, Keyboards, Clarinet: **Lara Martinovic**
- Composition, Guitars, Bass, Arrangements, Production: **Olivier Cléro**

Special guests:

- Violin: **Catherine Jacob**
- Cello: **Barbara Rialland**
- Fender Rhodes: **Louis Couka**

Album cover:

- Cover picture: [**Émilie Vernerey**](https://www.instagram.com/emilie.vernerey/)
- Design: **Olivier Cléro**
