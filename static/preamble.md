Software Enginer & Designer at [Guitar&nbsp;Pro](https://www.guitar-pro.com).

Also involved in [Solarus](https://www.solarus-games.org), a free and open-source 2D game engine.

Additionnally, musician for [Allflowers](https://www.allflowers-music.com) and comics artist.

Feel free to contact me!
